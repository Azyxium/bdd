-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: zzz_xxxxx_NOM_PRENOM_INFO1X_SUJET_104_2021

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS bahout_thibaud_info1d_comparateur_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS bahout_thibaud_info1d_comparateur_104_2021;

-- Utilisation de cette base de donnée

USE bahout_thibaud_info1d_comparateur_104_2021;
-- --------------------------------------------------------

--
-- Structure de la table `t_film`
--

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 06 Mai 2021 à 08:57
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bahout_thibaud_info1d_comparateur_104_2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_bluetooth`
--

CREATE TABLE `t_bluetooth` (
  `Id` int(11) NOT NULL,
  `Version` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_bluetooth`
--

INSERT INTO `t_bluetooth` (`Id`, `Version`) VALUES
(1, '5.0');

-- --------------------------------------------------------

--
-- Structure de la table `t_carte_graphique`
--

CREATE TABLE `t_carte_graphique` (
  `Id` int(11) NOT NULL,
  `Modele` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_carte_graphique`
--

INSERT INTO `t_carte_graphique` (`Id`, `Modele`) VALUES
(1, 'RTX 3090'),
(2, 'GTX 1080 TI'),
(3, 'RTX 3080 MaxQ');

-- --------------------------------------------------------

--
-- Structure de la table `t_clavier`
--

CREATE TABLE `t_clavier` (
  `Id` int(11) NOT NULL,
  `Clavier` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_clavier`
--

INSERT INTO `t_clavier` (`Id`, `Clavier`) VALUES
(1, 'QWERTZ'),
(2, 'AZERTY');

-- --------------------------------------------------------

--
-- Structure de la table `t_pc`
--

CREATE TABLE `t_pc` (
  `Id` int(11) NOT NULL,
  `Modele` varchar(20) NOT NULL,
  `Processeur` int(11) NOT NULL,
  `Carte_graphique` varchar(40) NOT NULL,
  `Ram` varchar(20) NOT NULL,
  `Stockage` varchar(10) NOT NULL,
  `Resolution` varchar(15) NOT NULL,
  `Clavier` varchar(20) NOT NULL,
  `Wifi` varchar(10) NOT NULL,
  `Bluetooth` varchar(5) NOT NULL,
  `Tactile` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pc`
--

INSERT INTO `t_pc` (`Id`, `Modele`, `Processeur`, `Carte_graphique`, `Ram`, `Stockage`, `Resolution`, `Clavier`, `Wifi`, `Bluetooth`, `Tactile`) VALUES
(1, 'Titan Pro Max', 1, 'RTX 4090TI', '2TB', '800TB', '7680 × 4320', 'QWERTY', 'XX', '7.2', 1);

-- --------------------------------------------------------

--
-- Structure de la table `t_processeur`
--

CREATE TABLE `t_processeur` (
  `Id_processeur` int(11) NOT NULL,
  `Modele` varchar(42) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_processeur`
--

INSERT INTO `t_processeur` (`Id_processeur`, `Modele`) VALUES
(1, 'i7 10700K'),
(2, 'Ryzen 9 3900X');

-- --------------------------------------------------------

--
-- Structure de la table `t_wifi`
--

CREATE TABLE `t_wifi` (
  `Id` int(11) NOT NULL,
  `Version` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_wifi`
--

INSERT INTO `t_wifi` (`Id`, `Version`) VALUES
(1, 'AC'),
(2, 'AX');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_bluetooth`
--
ALTER TABLE `t_bluetooth`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `t_carte_graphique`
--
ALTER TABLE `t_carte_graphique`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `t_clavier`
--
ALTER TABLE `t_clavier`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `t_pc`
--
ALTER TABLE `t_pc`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Processeur` (`Processeur`);

--
-- Index pour la table `t_processeur`
--
ALTER TABLE `t_processeur`
  ADD PRIMARY KEY (`Id_processeur`);

--
-- Index pour la table `t_wifi`
--
ALTER TABLE `t_wifi`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_bluetooth`
--
ALTER TABLE `t_bluetooth`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_carte_graphique`
--
ALTER TABLE `t_carte_graphique`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_clavier`
--
ALTER TABLE `t_clavier`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_pc`
--
ALTER TABLE `t_pc`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_processeur`
--
ALTER TABLE `t_processeur`
  MODIFY `Id_processeur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_wifi`
--
ALTER TABLE `t_wifi`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_pc`
--
ALTER TABLE `t_pc`
  ADD CONSTRAINT `t_pc_ibfk_1` FOREIGN KEY (`Processeur`) REFERENCES `t_processeur` (`Id_processeur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
